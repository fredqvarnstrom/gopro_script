"""
Python script for streaming live video from GoPro H4
"""
import subprocess
import urllib2

PATHTOFILE = "http://10.5.5.9:8080/live/amba.m3u8"
STREAMID = "zegf-pc3c-6bzb-cs55"
PASSWORD = "7096572120"

print "Enable Preview..."
url = "http://10.5.5.9/camera/PV?t=${}&p=%02".format(PASSWORD)
urllib2.urlopen(url, timeout=10)

cmd = "vlc \"%s\" -Idummy --network-caching 4000 --sout " \
      "'#transcode{vcodec=FLV1,acodec=mp3,samplerate=11025,threads=2,fps=25}:std{access=rtmp,mux=ffmpeg{mux=flv}," \
      "dst=rtmp://a.rtmp.youtube.com/live2/%s" % (PATHTOFILE, STREAMID)

subprocess.Popen(cmd, shell=True)
