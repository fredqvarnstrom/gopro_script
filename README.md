# GoPro H3 Camera Controller

## Clone the repository

    cd ~
    git clone https://bitbucket.org/rpi_guru/gopro_script


## Install dependencies

    cd gopro_script
    sudo pip install -r requirements.txt

## Execute script

Before executing, please make sure that your RPi is connected to the GoPro Camera thru wifi.

    python3 main.py

## Enable auto-starting

    sudo cp gopro.sh /etc/network/if-up.d/
    sudo chmod 755 /etc/network/if-up.d/gopro.sh
